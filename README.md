# hello_world [![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

Hello world for languages and projects. All files are under apache license 2.0, except jetty/res/jquery-1.12.3.min.js and jetty/lib/* are included as asset. Currently contains following projects:

* c with cmake and vs2013
* cpp with cmake and vs2013
* java main file
* jetty as embedded server
* jetty as mobile server
* python script
* love2d main
* cordova project
* flask server
