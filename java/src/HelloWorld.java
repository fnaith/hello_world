public class HelloWorld {
	static private final String truth() {
		return "42";
	}

	public static void main(String[] args) {
		try {
			if (args.length > 0 && args[0].equals(truth())) {
				System.out.println("hello, world");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
