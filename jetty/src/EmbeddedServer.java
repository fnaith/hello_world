import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class EmbeddedServer {
	static public class BaseResourceHandler extends ResourceHandler {
		@Override
		public void handle(String target, Request baseRequest,
				HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
			super.handle(target, baseRequest, request, response);
			System.out.print(request.getMethod());
			System.out.print(": ");
			System.out.print(target);
			System.out.print("\n");
		}
	}

	static public void main(String[] args) throws Exception {
		try {
			final int port = 8080;
			final String rootFolder = "res";

			Server server = new Server(port);
			ResourceHandler resourceHandler = new BaseResourceHandler();
			resourceHandler.setResourceBase(rootFolder);
			resourceHandler.setDirectoriesListed(true);
			resourceHandler.setWelcomeFiles(new String[] { "index.html" });

			ServletContextHandler context = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);
			context.setResourceBase(rootFolder);
			ServletHolder sh = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
			sh.setInitOrder(1);
			sh.setInitParameter("jersey.config.server.provider.packages", "rest");

			HandlerCollection handlerCollection = new HandlerCollection();
			handlerCollection.setHandlers(new Handler[] {resourceHandler, context});

			server.setHandler(handlerCollection);

			server.start();
			System.out.println("Server start");
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
