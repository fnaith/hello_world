package rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/hello")
public class HelloWorld {
	@GET
	@Path("/world")
	@Produces("text/plain")
	public String print() {
		System.out.println(666);
		return "hello, world";
	}
}
