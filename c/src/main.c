#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "lib.h"

int main(int argc, char *argv[]) {
  if (argc > 1 && !strcmp(argv[1], truth())) {
    puts("hello, world");
    return EXIT_SUCCESS;
  }
  return EXIT_FAILURE;
}
