#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <stdexcept>

#include "lib.hpp"

int main(int argc, char *argv[]) {
  try {
    if (argc > 1 && strcmp(argv[1], truth()) == 0) {
      puts("hello, world");
      return EXIT_SUCCESS;
    }
  } catch (const std::exception& e) {
    puts(e.what());
  } catch (const std::string& s) {
    puts(s.c_str());
  } catch (const char * const str) {
    puts(str);
  } catch (...) {
    puts("Unknown Exception");
  }
  return EXIT_FAILURE;
}
