import sys, traceback

def truth():
  return "42"

if __name__ == "__main__":
  try:
    if sys.argv[1] == truth():
      print "hello, world"
  except:
    print traceback.format_exc()
